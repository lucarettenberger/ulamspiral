class Icositetragon:

    def __init__(self,start):
        self.start = start

    def get(self,i,j):
        """
        get ico at i,j _-> IC_{ij} = (j+1) + 24*(i+start)
        :param i: row (e.g. row 0 = [0,1,2,3...,23])
        :param j: column (e.g. column 0 = [0,24,49,73,...])
        :return: value at i,j
        """
        if j>24:
            raise Exception('Column of the icositetragon can\'t be bigger than 24.')
        return (j+1) + 24*(i+self.start)

    def print(self,length):
        for i in range(0, length):
            for j in range(0, 24):
                val = self.get(i, j)
                if val < 10:
                    print(val, end='     ')
                if val >= 10 and val < 100:
                    print(val, end='    ')
                if val >= 100 and val < 1000:
                    print(val, end='   ')
                if val >= 1000:
                    print(val, end='  ')
            print()
