import matplotlib.pyplot as plt
from Icositetragon import Icositetragon
from PrimeListGenerator import PrimeListGenerator
from sklearn.cluster import KMeans
import numpy as np
from PIL import Image


from matplotlib import pyplot as plt
import numpy as np
from scipy.cluster.hierarchy import dendrogram
from imagecluster import calc as ic
from imagecluster import io as icio
from imagecluster import calc, io as icio, postproc



# The bottleneck is calc.fingerprints() called in this function, all other
# operations are very fast. get_image_data() writes fingerprints to disk and
# loads them again instead of re-calculating them.
images,fingerprints,timestamps = icio.get_image_data('ulams/')

# Run clustering on the fingerprints. Select clusters with similarity index
sim = 0.72

clusters = calc.cluster(fingerprints, sim=sim)

# Create dirs with links to images. Dirs represent the clusters the images
# belong to.
postproc.make_links(clusters, 'ulams/imagecluster/clusters')

# Plot images arranged in clusters.
postproc.visualize(clusters, images)

exit()


images,fingerprints,timestamps = icio.get_image_data('ulams_15/')
clusters,extra = ic.cluster(fingerprints, sim=0.59, extra_out=True)

# linkage matrix Z
fig,ax = plt.subplots()
dendrogram(extra['Z'], ax=ax)

# Adjust yaxis labels (values from Z[:,2]) to our definition of the `sim`
# parameter.
ymin, ymax = ax.yaxis.get_data_interval()
tlocs = np.linspace(ymin, ymax, 5)
ax.yaxis.set_ticks(tlocs)
tlabels = np.linspace(1, 0, len(tlocs))
ax.yaxis.set_ticklabels(tlabels)
ax.set_xlabel("image index")
ax.set_ylabel("sim")

fig.savefig('dendrogram.png')
plt.show()

exit()



for i in range(1, 410000, 50):
    print(i/410000)
    size = 50
    start_row = i

    primeList = PrimeListGenerator(((24*start_row)+1),size*24)
    ico = Icositetragon(start_row)

    mod24Matrix = list()
    feature = list()
    for k in range(size):
        row = list()
        #for j in [0,4,6,10,12,16,18,22]:
        for j in range(24):
            value = ico.get(k, j)
            if primeList.get_absolute(value) == 0:
                row.append(255)
                feature.append(0)
            else:
                row.append(0)
                feature.append(1)
        mod24Matrix.append(row)
    image = Image.fromarray(np.array(mod24Matrix,dtype=np.uint8))
    image.save('ulams/size_%d_start_%d.bmp' % (size, i))
    print()

