import numpy as np
from PIL import Image
import os
from matplotlib import pyplot as plt



allgemeins = list()
allgemeins.append([244,np.loadtxt('/Users/luca/PycharmProjects/ulamspiral/ulams/imagecluster/clusters/cluster_with_244/allgemein.txt')])
allgemeins.append([103,np.loadtxt('/Users/luca/PycharmProjects/ulamspiral/ulams/imagecluster/clusters/cluster_with_103/allgemein.txt')])
allgemeins.append([102,np.loadtxt('/Users/luca/PycharmProjects/ulamspiral/ulams/imagecluster/clusters/cluster_with_102/allgemein.txt')])
allgemeins.append([97,np.loadtxt('/Users/luca/PycharmProjects/ulamspiral/ulams/imagecluster/clusters/cluster_with_97/allgemein.txt')])

aller_algemeins = np.zeros(allgemeins[0][1].shape)

for allg in allgemeins:
    weight = allg[0]
    matrix = allg[1]
    for i in range(matrix.shape[0]):
        for j in range(matrix.shape[1]):
            aller_algemeins[i][j]+=(matrix[i][j]*weight)

plt.imshow(aller_algemeins)
plt.show()



exit()

directory = "/Users/luca/PycharmProjects/ulamspiral/ulams/imagecluster/clusters/cluster_with_40/cluster_0/"
average_matrix = np.zeros((50,24))
num_of_members = 0
for file in os.listdir(directory):
    num_of_members+=1
    img = Image.open(directory+file)
    arr = np.array(img) # 640x480x4 array
    binary_matrix = np.zeros(arr.shape)
    for i in range(arr.shape[0]):
        for j in range(arr.shape[1]):
            if arr[i][j] == 255:
                binary_matrix[i][j] = 0
            else:
                binary_matrix[i][j] = 1
    average_matrix = np.add(average_matrix,binary_matrix)
average_matrix = average_matrix/num_of_members

plt.imshow(average_matrix)
plt.show()

prime_colums = (average_matrix[:, [0, 4, 6, 10, 12, 16, 18, 22]])
prime_values_flatten = prime_colums.flatten()

p = 25
percentile = np.percentile(prime_values_flatten,p)
while p<99:
    prime_copy = np.copy(average_matrix)
    prime_copy[prime_copy < percentile] = 0
    plt.title(percentile)
    plt.imshow(prime_copy)
    plt.show()
    p+=5
    percentile = np.percentile(prime_values_flatten, p)

percentile = np.percentile(prime_values_flatten, 95)
prime_copy = np.copy(average_matrix)
prime_copy[prime_copy < percentile] = 0
plt.title(percentile)
plt.imshow(prime_copy)
plt.show()

np.savetxt(directory+'../allgemein.txt',prime_copy,fmt='%f')