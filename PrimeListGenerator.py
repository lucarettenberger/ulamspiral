class PrimeListGenerator:


    def __init__(self,start,size):
        # read primes from primes.txt file
        lines = [line.rstrip('\n') for line in open('primes.txt')]
        self.data = list()
        self.primes = list()
        self.start = start
        for line in lines:
            self.primes.append(int(line))
        self.primeList = list()
        for i in range(start,start+size+1):
            if i in self.primes:
                self.primeList.append(1)
            else:
                self.primeList.append(0)

    def get_absolute(self,i):
        return self.primeList[i-self.start]

    def get(self,i):
        return self.primeList[i]
